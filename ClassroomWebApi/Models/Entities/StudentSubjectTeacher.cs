namespace ClassroomWebApi.Models.Entities
{
    public class StudentSubjectTeacher
    {
        public int TeacherSubjectsId { get; set; }
        public TeacherSubject TeacherSubjects { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}