using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassroomWebApi.Models.Entities
{
    public class Student : AuditableEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]

        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }


        public DateTime DOB { get; set; }

        public int Age { get; set; }

        [Required]
        [StringLength(50)]
        public string ContactPerson { get; set; }

        [Required]
        [StringLength(20)]
        public string ContactNumber { get; set; }


        [Required]
        public int ClassroomId { get; set; }
        public Classroom Classroom { get; set; }

        public List<StudentSubjectTeacher> StudentSubjectTeachers { get; set; }
    }
}