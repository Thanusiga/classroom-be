using System;

namespace ClassroomWebApi.Models.Entities
{
    public abstract class AuditableEntity
    {
        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ModifiedAt { get; set; }
    }
}