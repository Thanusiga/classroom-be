using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassroomWebApi.Models.Entities
{
    public class TeacherSubject
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        public int SubjectId { get; set; }

        public Subject Subject { get; set; }
        public List<StudentSubjectTeacher> StudentSubjectTeachers { get; set; }

    }
}