using Microsoft.EntityFrameworkCore;
using ClassroomWebApi.Models.Entities;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;

namespace ClassroomWebApi.Models
{
    public class ClassroomContext : DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ClassroomContext(DbContextOptions<ClassroomContext> options,
        IHttpContextAccessor httpContextAccessor)
            : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Classroom> Classrooms { get; set; }
        public DbSet<TeacherClassroom> TeacherClassroom { get; set; }
        public DbSet<TeacherSubject> TeacherSubject { get; set; }
        public DbSet<StudentSubjectTeacher> StudentSubjectTeacher { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<TeacherSubject>()
                .HasOne(i => i.Subject)
                .WithMany(i => i.TeacherSubjects)
                .HasForeignKey(i => i.SubjectId);

            builder.Entity<TeacherSubject>()
                        .HasOne(i => i.Teacher)
                        .WithMany(i => i.TeacherSubjects)
                        .HasForeignKey(i => i.TeacherId);


            builder.Entity<TeacherClassroom>()
               .HasKey(i => new
               {
                   i.ClassroomId,
                   i.TeacherId
               });

            builder.Entity<TeacherClassroom>()
                .HasOne(i => i.Classroom)
                .WithMany(i => i.TeacherClassrooms)
                .HasForeignKey(i => i.ClassroomId);

            builder.Entity<TeacherClassroom>()
                        .HasOne(i => i.Teacher)
                        .WithMany(i => i.TeacherClassrooms)
                        .HasForeignKey(i => i.TeacherId);

            builder.Entity<StudentSubjectTeacher>()
            .HasKey(i => new
            {
                i.StudentId,
                i.TeacherSubjectsId
            });

            builder.Entity<StudentSubjectTeacher>()
                .HasOne(i => i.Student)
                .WithMany(i => i.StudentSubjectTeachers)
                .HasForeignKey(i => i.StudentId);

            builder.Entity<StudentSubjectTeacher>()
                        .HasOne(i => i.TeacherSubjects)
                        .WithMany(i => i.StudentSubjectTeachers)
                        .HasForeignKey(i => i.TeacherSubjectsId);
        }


        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // Get all the entities that inherit from AuditableEntity
            // and have a state of Added or Modified
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is AuditableEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            // For each entity we will set the Audit properties
            foreach (var entityEntry in entries)
            {
                // If the entity state is Added let's set
                // the CreatedAt and CreatedBy properties
                if (entityEntry.State == EntityState.Added)
                {
                    ((AuditableEntity)entityEntry.Entity).CreatedAt = DateTime.UtcNow;
                    ((AuditableEntity)entityEntry.Entity).CreatedBy = _httpContextAccessor?.HttpContext?.User?.Identity?.Name ?? "Admin_Thanu";
                }
                else
                {
                    // If the state is Modified then we don't want
                    // to modify the CreatedAt and CreatedBy properties
                    // so we set their state as IsModified to false
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedAt).IsModified = false;
                    Entry((AuditableEntity)entityEntry.Entity).Property(p => p.CreatedBy).IsModified = false;
                }

                // In any case we always want to set the properties
                // ModifiedAt and ModifiedBy
                ((AuditableEntity)entityEntry.Entity).ModifiedAt = DateTime.UtcNow;
                ((AuditableEntity)entityEntry.Entity).ModifiedBy = _httpContextAccessor?.HttpContext?.User?.Identity?.Name ?? "Admin_Thanu";
            }

            // After we set all the needed properties
            // we call the base implementation of SaveChangesAsync
            // to actually save our entities in the database
            return await base.SaveChangesAsync(cancellationToken);
        }

    }
}