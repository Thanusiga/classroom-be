using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.ViewModel
{
    public class StudentVm
    {
        public int Id { get; set; }



        public string FirstName { get; set; }


        public string LastName { get; set; }


        public string Email { get; set; }


        public string DOB { get; set; }

        public int Age { get; set; }


        public string ContactPerson { get; set; }


        public string ContactNumber { get; set; }



        public int ClassroomId { get; set; }
        public ClassroomVm Classroom { get; set; }
        public List<StudentSubjectTeacher> StudentSubjectTeachers { get; set; }

    }
}