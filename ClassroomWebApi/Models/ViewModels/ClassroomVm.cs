using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.ViewModel
{
    public class ClassroomVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public List<TeacherClassroom> TeacherClassrooms { get; set; }

    }
}