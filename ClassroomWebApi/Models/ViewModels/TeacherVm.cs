using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.ViewModel
{
    public class TeacherVm
    {
        public int Id { get; set; }


        public string FirstName { get; set; }


        public string LastName { get; set; }


        public string Email { get; set; }


        public string ContactNumber { get; set; }

        public List<TeacherSubject> TeacherSubjects { get; set; }
        public List<TeacherClassroom> TeacherClassrooms { get; set; }

    }
}