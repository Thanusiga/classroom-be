using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.ViewModel
{
    public class SubjectVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<TeacherSubject> TeacherSubjects { get; set; }
    }
}