using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.Dto.Teacher
{
    public class TeacherUpdateDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]

        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string Email { get; set; }


        public string ContactNumber { get; set; }

        public List<TeacherSubject> TeacherSubjects { get; set; }
        public List<TeacherClassroom> TeacherClassrooms { get; set; }
    }

    public class TeacherSubjectDto
    {
        public int TeacherId { get; set; }


        public int SubjectId { get; set; }

    }

    public class TeacherClassroomDto
    {

        public int TeacherId { get; set; }


        public int ClassroomId { get; set; }

    }
}