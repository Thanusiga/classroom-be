using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.Dto.Teacher
{
    public class TeacherDto
    {

        [Required]
        [StringLength(50)]

        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }


        public string ContactNumber { get; set; }
        public List<int> SubjectIds { get; set; }
        public List<int> ClassRoomIds { get; set; }



    }
}