using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.Dto.Classroom
{
    public class ClassroomDto
    {
        [Required]
        public string Name { get; set; }

    }
}