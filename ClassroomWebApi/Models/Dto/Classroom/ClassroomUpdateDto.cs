using System.ComponentModel.DataAnnotations;

namespace ClassroomWebApi.Models.Dto.Classroom
{
    public class ClassroomUpdateDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}