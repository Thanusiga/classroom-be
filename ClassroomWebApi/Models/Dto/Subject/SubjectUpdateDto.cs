using System.ComponentModel.DataAnnotations;

namespace ClassroomWebApi.Models.Dto.Subject
{
    public class SubjectUpdateDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }


    }
}