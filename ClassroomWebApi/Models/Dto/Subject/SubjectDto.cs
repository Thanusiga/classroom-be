using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.Dto.Subject
{
    public class SubjectDto
    {

        [Required]
        public string Name { get; set; }



    }
}