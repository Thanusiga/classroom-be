using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ClassroomWebApi.Models.Entities;

namespace ClassroomWebApi.Models.Dto.Student
{
    public class StudentUpdateDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]

        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string Email { get; set; }


        public DateTime DOB { get; set; }

        // public int Age { get; set; }

        [Required]
        [StringLength(50)]
        public string ContactPerson { get; set; }

        [Required]
        [StringLength(20)]
        public string ContactNumber { get; set; }


        [Required]
        public int ClassroomId { get; set; }

        public List<StudentSubjectTeacher> StudentSubjectTeachers { get; set; }


    }
}