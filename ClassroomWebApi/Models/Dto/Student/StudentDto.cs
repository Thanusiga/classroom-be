using System.Collections.Generic;
using ClassroomWebApi.Models.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassroomWebApi.Models.Dto.Student
{
    public class StudentDto
    {

        [Required]
        [StringLength(50)]

        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }


        public DateTime DOB { get; set; }


        [Required]
        [StringLength(50)]
        public string ContactPerson { get; set; }

        [Required]
        [StringLength(20)]
        public string ContactNumber { get; set; }


        [Required]
        public int ClassroomId { get; set; }

        public List<int> TeacherSubjectsIds { get; set; }

    }
}