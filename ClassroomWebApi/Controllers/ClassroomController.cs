using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace ClassroomWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClassroomController : ControllerBase
    {
        private readonly ClassroomContext _context;
        private readonly IClassroom _classroomService;

        public ClassroomController(ClassroomContext contex, IClassroom classroomService)
        {
            _context = contex;
            _classroomService = classroomService;
        }

        //GET: api/classroom
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Classroom>>> GetAll()
        {

            var classroom = await _classroomService.GetAll();

            return Ok(classroom);
        }

        //GET: api/classroom/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<ClassroomVm>> GetById(int id)
        {

            var classroom = await _classroomService.GetById(id);

            return Ok(classroom);
        }

        //POST: api/classroom
        [HttpPost]
        public async Task<ActionResult<ClassroomVm>> Create([FromBody] ClassroomDto dto)
        {

            var classroom = await _classroomService.Create(dto);

            return CreatedAtAction(nameof(Create), new { id = classroom.Id }, classroom);

        }

        //POST: api/classroom
        [HttpPut("{id}")]
        public async Task<ActionResult<ClassroomVm>> Update(int id, [FromBody] ClassroomUpdateDto dto)
        {
            dto.Id = id;
            var classroom = await _classroomService.Update(dto);

            return Ok(classroom);
        }

        //Delete: api/classroom/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClassroomVm>> Delete(int id)
        {

            var classroom = await _classroomService.Delete(id);

            return NoContent();
        }
    }
}