using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Dto.Subject;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
namespace ClassroomWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubjectController : ControllerBase
    {

        private readonly ClassroomContext _context;
        private readonly ISubject _subjectService;

        public SubjectController(ClassroomContext contex, ISubject subjectService)
        {
            _context = contex;
            _subjectService = subjectService;
        }

        //GET: api/subject
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Subject>>> GetAll()
        {

            var subject = await _subjectService.GetAll();

            return Ok(subject);
        }

        //GET: api/subject/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<SubjectVm>> GetById(int id)
        {

            var subject = await _subjectService.GetById(id);

            return Ok(subject);
        }

        //POST: api/subject
        [HttpPost]
        public async Task<ActionResult<SubjectVm>> Create([FromBody] SubjectDto dto)
        {

            var subject = await _subjectService.Create(dto);

            return CreatedAtAction(nameof(Create), new { id = subject.Id }, subject);
        }

        //POST: api/subject
        [HttpPut("{id}")]
        public async Task<ActionResult<SubjectVm>> Update(int id, [FromBody] SubjectUpdateDto dto)
        {
            dto.Id = id;

            var subject = await _subjectService.Update(dto);

            return Ok(subject);
        }

        //Delete: api/subject/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult<SubjectVm>> Delete(int id)
        {

            var subject = await _subjectService.Delete(id);

            return NoContent();
        }


        //GET: api/subject
        [HttpGet("get-sub-with-teachers")]
        public async Task<ActionResult<IEnumerable<Subject>>> GetAllSubWithTeacher()
        {

            var subject = await _subjectService.GetAllSubWithTeacher();

            return Ok(subject);
        }
    }
}