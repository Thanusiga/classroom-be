using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Dto.Student;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
namespace ClassroomWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : ControllerBase
    {
        private readonly ClassroomContext _context;
        private readonly IStudent _studentService;

        public StudentController(ClassroomContext contex, IStudent studentService)
        {
            _context = contex;
            _studentService = studentService;
        }

        //GET: api/student
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentVm>>> GetAll()
        {

            var student = await _studentService.GetAll();

            return Ok(student);
        }

        //GET: api/student/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentVm>> GetById(int id)
        {

            var student = await _studentService.GetById(id);

            return Ok(student);
        }

        //POST: api/student
        [HttpPost]
        public async Task<ActionResult<StudentVm>> Create([FromBody] StudentDto dto)
        {

            var student = await _studentService.Create(dto);

            return CreatedAtAction(nameof(Create), new { id = student.Id }, student);
        }

        //POST: api/student
        [HttpPut("{id}")]
        public async Task<ActionResult<StudentVm>> Update(int id, [FromBody] StudentUpdateDto dto)
        {
            dto.Id = id;
            var student = await _studentService.Update(dto);

            return Ok(student);
        }

        //Delete: api/student/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult<StudentVm>> Delete(int id)
        {

            var student = await _studentService.Delete(id);

            return NoContent();
        }
    }
}