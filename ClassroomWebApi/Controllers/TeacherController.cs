using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Dto.Teacher;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
namespace ClassroomWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeacherController : ControllerBase
    {

        private readonly ClassroomContext _context;
        private readonly ITeacher _teacherService;

        public TeacherController(ClassroomContext contex, ITeacher teacherService)
        {
            _context = contex;
            _teacherService = teacherService;
        }

        //GET: api/teacher
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Teacher>>> GetAll()
        {

            var teacher = await _teacherService.GetAll();

            return Ok(teacher);
        }

        //GET: api/teacher/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<TeacherVm>> GetById(int id)
        {

            var teacher = await _teacherService.GetById(id);

            return Ok(teacher);
        }

        //POST: api/teacher
        [HttpPost]
        public async Task<ActionResult<TeacherVm>> Create([FromBody] TeacherDto dto)
        {

            var teacher = await _teacherService.Create(dto);

            return CreatedAtAction(nameof(Create), new { id = teacher.Id }, teacher);
        }

        //POST: api/teacher
        [HttpPut("{id}")]
        public async Task<ActionResult<TeacherVm>> Update(int id, [FromBody] TeacherUpdateDto dto)
        {
            dto.Id = id;
            var teacher = await _teacherService.Update(dto);

            return Ok(teacher);
        }

        //Delete: api/teacher/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult<TeacherVm>> Delete(int id)
        {

            var teacher = await _teacherService.Delete(id);

            return NoContent();
        }
    }
}