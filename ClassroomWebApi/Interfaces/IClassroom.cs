using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.ViewModel;
using ClassroomWebApi.Models.Dto.Classroom;

namespace ClassroomWebApi.Interfaces
{
    public interface IClassroom
    {
        Task<List<Classroom>> GetAll();
        Task<Classroom> GetById(int id);
        Task<ClassroomVm> Create(ClassroomDto dto);
        Task<ClassroomVm> Update(ClassroomUpdateDto dto);
        Task<int> Delete(int id);

    }
}