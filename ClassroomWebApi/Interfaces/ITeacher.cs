using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.ViewModel;
using ClassroomWebApi.Models.Dto.Teacher;

namespace ClassroomWebApi.Interfaces
{
    public interface ITeacher
    {
        Task<List<Teacher>> GetAll();
        Task<Teacher> GetById(int id);
        Task<TeacherVm> Create(TeacherDto dto);
        Task<TeacherVm> Update(TeacherUpdateDto dto);
        Task<int> Delete(int id);

    }
}