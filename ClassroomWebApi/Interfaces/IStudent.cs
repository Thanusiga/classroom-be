using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.ViewModel;
using ClassroomWebApi.Models.Dto.Student;

namespace ClassroomWebApi.Interfaces
{
    public interface IStudent
    {
        Task<List<StudentVm>> GetAll();
        Task<StudentVm> GetById(int id);
        Task<StudentVm> Create(StudentDto dto);
        Task<StudentVm> Update(StudentUpdateDto dto);
        Task<int> Delete(int id);

    }
}