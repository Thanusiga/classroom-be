using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.ViewModel;
using ClassroomWebApi.Models.Dto.Subject;

namespace ClassroomWebApi.Interfaces
{
    public interface ISubject
    {
        Task<List<Subject>> GetAll();
        Task<Subject> GetById(int id);
        Task<SubjectVm> Create(SubjectDto dto);
        Task<SubjectVm> Update(SubjectUpdateDto dto);
        Task<int> Delete(int id);

        Task<List<TeacherSubject>> GetAllSubWithTeacher();

    }
}