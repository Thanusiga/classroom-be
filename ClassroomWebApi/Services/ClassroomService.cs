using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace ClassroomWebApi.Services
{
    public class ClassroomService : IClassroom
    {
        private readonly ClassroomContext _context;

        public ClassroomService(ClassroomContext contex)
        {
            _context = contex;
        }

        public async Task<ClassroomVm> Create(ClassroomDto dto)
        {
            var isExist = await _context.Classrooms.FirstOrDefaultAsync(x => x.Name == dto.Name);
            if (isExist != null)
            {
                throw new System.Exception($"{dto.Name} is already exist");
            }

            var cls = new Classroom()
            {
                Name = dto.Name,
            };

            var classroom = _context.Classrooms.Add(cls);
            await _context.SaveChangesAsync();

            return new ClassroomVm()
            {
                Id = classroom.Entity.Id,
                Name = classroom.Entity.Name
            };
        }

        public async Task<int> Delete(int id)
        {
            var cls = await _context.Classrooms.FindAsync(id);

            if (cls == null)
            {
                throw new System.Exception($"{id} is does not exist");
            }

            _context.Classrooms.Remove(cls);

            return await _context.SaveChangesAsync();
        }



        public async Task<List<Classroom>> GetAll()
        {
            return await _context.Classrooms.ToListAsync();
        }

        public async Task<Classroom> GetById(int id)
        {
            return await _context.Classrooms.FindAsync(id);
        }

        public async Task<ClassroomVm> Update(ClassroomUpdateDto dto)
        {
            var classroom = await _context.Classrooms.FindAsync(dto.Id);

            if (classroom == null)
            {
                throw new System.NullReferenceException($"{dto.Id} is not found");
            }

            classroom.Name = dto.Name;
            _context.Classrooms.Update(classroom);
            await _context.SaveChangesAsync();

            return new ClassroomVm()
            {
                Id = classroom.Id,
                Name = classroom.Name
            };
        }
    }
}