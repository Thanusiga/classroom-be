using ClassroomWebApi.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
using ClassroomWebApi.Models.Dto.Student;
using System;
using System.Linq;

namespace ClassroomWebApi.Services
{
    public class StudentService : IStudent
    {
        private readonly ClassroomContext _context;

        public StudentService(ClassroomContext contex)
        {
            _context = contex;
        }

        public async Task<StudentVm> Create(StudentDto dto)
        {
            if (await _context.Students.AsNoTracking().FirstOrDefaultAsync(x => x.Email == dto.Email) != null)
            {
                throw new System.Exception($"{dto.Email} is already exist");
            }
            var age = DateTime.Now.Year - dto.DOB.Year;

            var cls = new Student()
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                DOB = dto.DOB,
                Age = age,
                ContactPerson = dto.ContactPerson,
                ContactNumber = dto.ContactNumber,
                ClassroomId = dto.ClassroomId,
            };

            var student = await _context.Students.AddAsync(cls);
            await _context.SaveChangesAsync();

            await _context.Entry<Student>(student.Entity).Reference(d => d.Classroom).LoadAsync();
            await _context.Entry<Student>(student.Entity)
                        .Collection(d => d.StudentSubjectTeachers)
                        .Query()
                        .Include(d => d.TeacherSubjects)
                        .LoadAsync();

            var stdSubTeaList = new List<StudentSubjectTeacher>();

            foreach (var sub in dto.TeacherSubjectsIds)
            {
                var stdSubTea = new StudentSubjectTeacher()
                {
                    StudentId = student.Entity.Id,
                    TeacherSubjectsId = sub,
                };

                stdSubTeaList.Add(stdSubTea);
            }

            if (stdSubTeaList != null && stdSubTeaList.Count > 0)
            {
                student.Entity.StudentSubjectTeachers = stdSubTeaList;
                _context.Students.Update(student.Entity);

                await _context.SaveChangesAsync();

            }

            return new StudentVm()
            {
                Id = student.Entity.Id,
                FirstName = student.Entity.FirstName,
                LastName = student.Entity.LastName,
                Email = student.Entity.Email,
                DOB = student.Entity.DOB.ToString("yyyy-MM-dd"),
                Age = age,
                ContactPerson = student.Entity.ContactPerson,
                ContactNumber = student.Entity.ContactNumber,
                ClassroomId = student.Entity.ClassroomId,
                Classroom = new ClassroomVm()
                {
                    Id = student.Entity.Classroom.Id,
                    Name = student.Entity.Classroom.Name,
                },
                StudentSubjectTeachers = student.Entity.StudentSubjectTeachers,
                // StudentSubjectTeachersIds = student.Entity.StudentSubjectTeachers.Select(x => x.I)

            };
        }

        public async Task<int> Delete(int id)
        {
            var cls = await _context.Students.FindAsync(id);

            if (cls == null)
            {
                throw new System.Exception($"{id} is does not exist");
            }

            _context.Students.Remove(cls);

            return await _context.SaveChangesAsync();
        }



        public async Task<List<StudentVm>> GetAll()
        {
            return await _context.Students.Include(x => x.Classroom).Select(
                x =>
                new StudentVm()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    DOB = x.DOB.ToString("yyyy-MM-dd"),
                    Age = DateTime.Now.Year - x.DOB.Year,
                    ContactPerson = x.ContactPerson,
                    ContactNumber = x.ContactNumber,
                    ClassroomId = x.ClassroomId,
                    Classroom = new ClassroomVm()
                    {
                        Id = x.Classroom.Id,
                        Name = x.Classroom.Name,
                    },
                    StudentSubjectTeachers = x.StudentSubjectTeachers,
                }
            ).ToListAsync();
        }

        public async Task<StudentVm> GetById(int id)
        {
            var student = await _context.Students
            .Include(x => x.Classroom)
             .Include(x => x.StudentSubjectTeachers)
            .ThenInclude(z => z.TeacherSubjects)
            .ThenInclude(c => c.Teacher)
            .Include(x => x.StudentSubjectTeachers)
            .ThenInclude(z => z.TeacherSubjects)
            .ThenInclude(c => c.Subject).FirstOrDefaultAsync(x => x.Id == id);

            if (student == null)
            {
                throw new System.Exception($"{id} is does not exist");
            }

            var age = DateTime.Now.Year - student.DOB.Year;

            return new StudentVm()
            {
                Id = student.Id,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Email = student.Email,
                DOB = student.DOB.ToString("yyyy-MM-dd"),
                Age = age,
                ContactPerson = student.ContactPerson,
                ContactNumber = student.ContactNumber,
                ClassroomId = student.ClassroomId,
                Classroom = new ClassroomVm()
                {
                    Id = student.Classroom.Id,
                    Name = student.Classroom.Name,
                },
                StudentSubjectTeachers = student.StudentSubjectTeachers,

            };
        }

        public async Task<StudentVm> Update(StudentUpdateDto dto)
        {
            var student = await _context.Students.Include(x => x.Classroom)
            .Include(x => x.StudentSubjectTeachers)
            .ThenInclude(x => x.TeacherSubjects)
            .FirstOrDefaultAsync(x => x.Id == dto.Id);

            if (student == null)
            {
                throw new System.NullReferenceException($"{dto.Id} is not found");
            }

            var age = DateTime.Now.Year - dto.DOB.Year;

            student.Id = dto.Id;
            student.FirstName = dto.FirstName;
            student.LastName = dto.LastName;
            student.Email = dto.Email;
            student.DOB = dto.DOB;
            student.Age = age;
            student.ContactPerson = dto.ContactPerson;
            student.ContactNumber = dto.ContactNumber;
            student.ClassroomId = dto.ClassroomId;
            student.StudentSubjectTeachers = dto.StudentSubjectTeachers;


            _context.Students.Update(student);
            await _context.SaveChangesAsync();


            return new StudentVm()
            {
                Id = student.Id,
                LastName = student.LastName,
                Email = student.Email,
                DOB = student.DOB.ToString("yyyy-MM-dd"),
                Age = age,
                ContactPerson = student.ContactPerson,
                ContactNumber = student.ContactNumber,
                ClassroomId = student.ClassroomId,
                Classroom = new ClassroomVm()
                {
                    Id = student.Classroom.Id,
                    Name = student.Classroom.Name,
                },
                StudentSubjectTeachers = student.StudentSubjectTeachers,
            };
        }
    }
}