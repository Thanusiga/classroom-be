using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Interfaces;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Dto.Teacher;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
namespace ClassroomWebApi.Services
{
    public class TeacherService : ITeacher
    {
        private readonly ClassroomContext _context;

        public TeacherService(ClassroomContext contex)
        {
            _context = contex;
        }

        public async Task<TeacherVm> Create(TeacherDto dto)
        {
            if (await _context.Teachers.FirstOrDefaultAsync(x => x.Email == dto.Email) != null)
            {
                throw new System.Exception($"{dto.Email} is already exist");
            }

            var cls = new Teacher()
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                ContactNumber = dto.ContactNumber,
            };

            var teacher = _context.Teachers.Add(cls);
            await _context.SaveChangesAsync();


            var teaSubList = new List<TeacherSubject>();

            foreach (var sub in dto.SubjectIds)
            {
                var teaSub = new TeacherSubject()
                {
                    TeacherId = teacher.Entity.Id,
                    SubjectId = sub,
                };

                teaSubList.Add(teaSub);
            }

            if (teaSubList != null && teaSubList.Count > 0)
            {
                await _context.TeacherSubject.AddRangeAsync(teaSubList);

                await _context.SaveChangesAsync();

            }


            var teaClsList = new List<TeacherClassroom>();

            foreach (var clsrm in dto.ClassRoomIds)
            {
                var teaCls = new TeacherClassroom()
                {
                    TeacherId = teacher.Entity.Id,
                    ClassroomId = clsrm,
                };

                teaClsList.Add(teaCls);
            }

            if (teaClsList != null && teaClsList.Count > 0)
            {
                await _context.TeacherClassroom.AddRangeAsync(teaClsList);

                await _context.SaveChangesAsync();

            }


            return new TeacherVm()
            {
                Id = teacher.Entity.Id,
                FirstName = teacher.Entity.FirstName,
                LastName = teacher.Entity.LastName,
                Email = teacher.Entity.Email,
                ContactNumber = teacher.Entity.ContactNumber,
            };
        }

        public async Task<int> Delete(int id)
        {
            var cls = await _context.Teachers.FindAsync(id);

            if (cls == null)
            {
                throw new System.Exception($"{id} is does not exist");
            }

            _context.Teachers.Remove(cls);

            return await _context.SaveChangesAsync();
        }



        public async Task<List<Teacher>> GetAll()
        {
            return await _context.Teachers.ToListAsync();
        }

        public async Task<Teacher> GetById(int id)
        {
            return await _context.Teachers.FindAsync(id);
        }

        public async Task<TeacherVm> Update(TeacherUpdateDto dto)
        {
            var teacher = await _context.Teachers.Include(x => x.TeacherClassrooms)
            .Include(y => y.TeacherSubjects).FirstOrDefaultAsync(z => z.Id == dto.Id);

            if (teacher == null)
            {
                throw new System.NullReferenceException($"{dto.Id} is not found");
            }


            teacher.Id = dto.Id;
            teacher.FirstName = dto.FirstName;
            teacher.LastName = dto.LastName;
            teacher.Email = dto.Email;
            teacher.ContactNumber = dto.ContactNumber;
            teacher.TeacherClassrooms = dto.TeacherClassrooms;
            teacher.TeacherSubjects = dto.TeacherSubjects;


            _context.Teachers.Update(teacher);
            await _context.SaveChangesAsync();

            return new TeacherVm()
            {
                Id = teacher.Id,
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                Email = teacher.Email,
                ContactNumber = teacher.ContactNumber,
            };
        }
    }

}