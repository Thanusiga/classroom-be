using ClassroomWebApi.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using ClassroomWebApi.Models;
using ClassroomWebApi.Models.Dto;
using ClassroomWebApi.Models.Dto.Classroom;
using ClassroomWebApi.Models.Entities;
using ClassroomWebApi.Models.ViewModel;
using Microsoft.EntityFrameworkCore;
using ClassroomWebApi.Models.Dto.Subject;

namespace ClassroomWebApi.Services
{
    public class SubjectService : ISubject
    {
        private readonly ClassroomContext _context;

        public SubjectService(ClassroomContext contex)
        {
            _context = contex;
        }

        public async Task<SubjectVm> Create(SubjectDto dto)
        {
            if (await _context.Subjects.FirstOrDefaultAsync(x => x.Name == dto.Name) != null)
            {
                throw new System.Exception($"{dto.Name} is already exist");
            }

            var cls = new Subject()
            {
                Name = dto.Name,
            };

            var subject = _context.Subjects.Add(cls);
            await _context.SaveChangesAsync();

            return new SubjectVm()
            {
                Id = subject.Entity.Id,
                Name = subject.Entity.Name
            };
        }

        public async Task<int> Delete(int id)
        {
            var cls = await _context.Subjects.FindAsync(id);

            if (cls == null)
            {
                throw new System.Exception($"{id} is does not exist");
            }

            _context.Subjects.Remove(cls);

            return await _context.SaveChangesAsync();
        }



        public async Task<List<Subject>> GetAll()
        {
            return await _context.Subjects.ToListAsync();
        }

        public async Task<Subject> GetById(int id)
        {
            return await _context.Subjects.FindAsync(id);
        }

        public async Task<SubjectVm> Update(SubjectUpdateDto dto)
        {
            var subject = await _context.Subjects.FindAsync(dto.Id);

            if (subject == null)
            {
                throw new System.NullReferenceException($"{dto.Id} is not found");
            }

            subject.Id = dto.Id;
            subject.Name = dto.Name;

            _context.Subjects.Update(subject);


            await _context.SaveChangesAsync();

            return new SubjectVm()
            {
                Id = subject.Id,
                Name = subject.Name
            };
        }
        // getAll subject with teach

        public async Task<List<TeacherSubject>> GetAllSubWithTeacher()
        {
            return await _context.TeacherSubject
            .Include(x => x.Teacher)
            .Include(x => x.Subject)
            .ToListAsync();
        }
    }
}