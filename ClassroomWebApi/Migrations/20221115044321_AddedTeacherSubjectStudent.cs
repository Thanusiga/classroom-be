﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ClassroomWebApi.Migrations
{
    public partial class AddedTeacherSubjectStudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_TeacherSubject",
                table: "TeacherSubject");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "TeacherSubject",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeacherSubject",
                table: "TeacherSubject",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "StudentSubjectTeacher",
                columns: table => new
                {
                    TeacherSubjectsId = table.Column<int>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubjectTeacher", x => new { x.StudentId, x.TeacherSubjectsId });
                    table.ForeignKey(
                        name: "FK_StudentSubjectTeacher_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSubjectTeacher_TeacherSubject_TeacherSubjectsId",
                        column: x => x.TeacherSubjectsId,
                        principalTable: "TeacherSubject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSubject_SubjectId",
                table: "TeacherSubject",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjectTeacher_TeacherSubjectsId",
                table: "StudentSubjectTeacher",
                column: "TeacherSubjectsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentSubjectTeacher");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TeacherSubject",
                table: "TeacherSubject");

            migrationBuilder.DropIndex(
                name: "IX_TeacherSubject_SubjectId",
                table: "TeacherSubject");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "TeacherSubject");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TeacherSubject",
                table: "TeacherSubject",
                columns: new[] { "SubjectId", "TeacherId" });
        }
    }
}
